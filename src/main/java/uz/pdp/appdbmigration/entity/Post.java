package uz.pdp.appdbmigration.entity;

import jakarta.persistence.*;

@Entity
public class Post {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String title;

    private String body;

    @ManyToOne(optional = false)
    @JoinColumn(name = "author_ID", nullable = false)
    private Author author;


}
