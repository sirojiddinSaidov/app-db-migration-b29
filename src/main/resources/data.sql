-- ALTER TABLE IF EXISTS author
--     RENAME COLUMN name TO full_name;

ALTER TABLE IF EXISTS author
    RENAME COLUMN full_name TO name;

ALTER TABLE IF EXISTS author
    ADD CONSTRAINT uk_author_name UNIQUE (name);
